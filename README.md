#  Welcome to Uncle's Lotto Picker! #

 *******************************
 ******* 1. INTRODUCTION *******
 *******************************
 

 This is the main entry point of the application
 We are going to be passed an argument which is the name of a text file in the same directory as our program
 This file will be a UTF-8 formatted, new line separated, list of strings of numbers
 EX:
    123750182
    1
    8587387208378378302837
    0284
 
 For each of these numbers, our program will discover if the number can be subdivided to create a lotto number (7 numbers long, all numbers from 1-59, all numbers must be used and must be used in order from left-to-right)




 *******************************
 ******** 2. STRATEGY **********
 *******************************

 We will be approaching this as a graph theory problem using a Depth First Search technique with some improvements that are unique to this problem.
 
 **** 2A. DFS ****
 Take as an example the string 45321633
 From the root of our graph there are 2 legal child nodes: (4) and (45). Each of these nodes will contains the remainder of the string which they came from so that they may continue their traversal without knowledge of the rest of the graph. Thus, our data will look someting like...
 
 Node:
    - value : 4
    - string : "5321633"
    - depth : 1
    - parent : (root Node ref)
 
 We will know a solution is found when the following conditions are true for a node
 
    - We are at a depth of exactly 7 (indicating we have 7 separate numbers for our pick)
    - Our "string" member is empty, indicating we have used every number in the string
 
 All we have to do then is print out our solution by traversing upwards until we hit the root by following our "parent" refs
 
 
 **** 2B. IMPROVEMENTS ****
 There are a number of improvements we can make to increase the speed of our program, given our particular constraints.
 
 1. We know that each number in a legal solution will be either 1 digit (1-9) or 2 digits (10-59). Knowing that each number given to us, in order to offer a legal solution, must give us 7 one-digit numbers, 7 two-digit numbers, or a mix of 7 one and two-digit numbers, we can immediately state that if a string (before traversal from the root begins) is less than 7 characters long or more than 14 characters long, it is impossible for us to find a solution and we can move on. This is because if we travers the total string we will never arrive at a depth of exactly 7.
 
 2. Because each lotto pick must be from 1-59 inclusive, We know that every Zero (0) in a string will only offer us a legal node value if it is preceeded by a 1, 2, 3, 4 or 5 allowing us to make up the numbers 10, 20, 30, 40, and 50.
    Ex: If we wish to find the lotto numbers in the string "41423160" there are many legal paths that all end up with nodes that have the remaining string "60". One example is "41 4 2 3 1". Because this particular tuple contains a number greater than 5 followed by a 0, there is no way to traverse to the end of the string. If we traverse to the next node with a value of "6" we are left with a hanging "0", and we cannot traverse legally to a node with "60" because it is greater than 59.
 
   The same conclusion follows for a string with two continguous Zero (0) characters, as 0 cannot be traversed by itself and in the event we are able to traverse past the first 0 becuase it is prepended by a 1-5, we are again left with a hanging, untraversable, 0.
 
   Thus we can state that any string containing a [0, 6, 7, 8 or 9] followed by a [0] will have 0 legal solutions as it will be untraversable.
 
 3. Related to the point above, we know that anytime we have a choice between traversing to a node with a single digit and traversing to a node with a double digit ending with a 0, WE MUST TRAVERSE THE DOUBLE DIGIT, otherwise we end up with a hanging 0. (Re: Implementation, Rather than doing string comparison to check for a 0, we can simply check if our number % 10 == 0)
 
 
 4. Given our assumptions from point 1 above, we can analyze a given string by its length in order to discover exactly how many single digit and 2 digit numbers are required to find a legal solution in that string.
    
     Say we have a string that is 14 characters long before traversal begins. We know that this string will require 7 separate 2 digit numbers in order for us to find a legal solution. Thus, we can ALWAYS ignore single digit numbers (anything less than 10) while making our traversal.
 
         In the case of a 7 character long string, we know that this string will require 7 separate 1 digit numbers. Thus we can ALWAYS ignore double digit numbers (anything greater than 9).
 
         This can be generalized into the following formula.
            GIVEN THAT
            - We have a string of length 'N'
            - 'N' is greater than 7 and less than 14 (meaning it is legal according to point 1 above)
            - Before traversal begins
            - Knowing that we MUST end up with 7 separate 1 or 2 digit numbers
            - The number of 1 digit numbers in our solution plus the number of 2 digit numbers in our solution must total 7
            - Every number in our solutions must use AT LEAST one digit
            - THE ENTIRETY of our original string must be used in our solution
            - Any number than does not use 1 digit MUST use 2 digits
            WE MAY CONCLUDE THAT
            - The number of 2 digit numbers required for any given solution derived from this string will ALWAYS BE EQUAL TO
                'N' - 7
            - Thus, the number of 1 digit numbers required for any given solution will always equal 7 - ('N' - 7)
    
    
    This helps us becuase it means that before we even begin traversal, we can know ahead of time how many single digit and two digits numbers will be required in this solution. We can keep this information in our root node and modify it as we go down each branch of our graph. 
 
    Node:
        - {...}
        - singlesRequired (int)
        - doublesRequired (int)
 
    Ex: 
        We begin with the string 504034567
        This is a string of length 9, thus from our formula we know the number of 2 digit numbers required is 
        equal to 9 - 7 = 2. Thus, the number of single digit numbers required is equal to 7 - 2 = 5.
 
        When we begin traversal, following from point 3 above we know that our first 2 nodes will be of value 50 and 40. Each time we make this traversal, because each of them are DOUBLE DIGIT NUMBERS, we DECREASE the double digits required field of our node.
 
        Node (our first node after root):
            - {...}
            - value : 50
            - depth : 1
            - singlesRequired : 5
            - doublesRequired : 1
 
        Node (our second node after root):
            - {...}
            - value : 40
            - depth : 1
            - singlesRequired : 5
            - doublesRequired : 0
 
        At this point we know that we should avoid ALL double digit numbers in our traversal down this branch, as we will not have enough numbers remaining at the end of our traversal.
 
 