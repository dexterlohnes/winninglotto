//
//  UnclesLottoUnitTests.swift
//  UnclesLottoUnitTests
//
//  Created by Dexter Lohnes on 11/17/16.
//  Copyright © 2016 DexterLohnes. All rights reserved.
//

import XCTest

class LottoNodeUnitTests: XCTestCase {
    
    var node : LottoNode!
    
    override func setUp() {
        super.setUp()
        node = LottoNode()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_WhenNodeHasRequiredDepthAndNoStringRemains_SolutionIsFound() {
        // Given
        let str = ""
        let depth = LottoNode.REQUIRED_DEPTH_FOR_SUCCESS
        let node : LottoNode = LottoNode(string: str, depth: depth)
        
        // When
        let isLottoNumber = node.ContainsLottoNumber()
        
        // Then
        XCTAssertTrue(isLottoNumber)
    }
    
    func test_WhenNodeHasRequiredDepthAndSomeStringRemains_SolutionIsNotFound() {
        // Given
        let str = "1"
        let depth = LottoNode.REQUIRED_DEPTH_FOR_SUCCESS
        let node : LottoNode = LottoNode(string: str, depth: depth)
        
        // When
        let isLottoNumber = node.ContainsLottoNumber()
        
        // Then
        XCTAssertFalse(isLottoNumber)
    }
    
    func test_WhenNodeHasDepthNotEqualToRequiredDepthAndNoRemains_SolutionIsNotFound() {
        
        // Given
        let str = ""
        // NOTE: We are skipping the required depth here (7, in this solution)
        let depthRange = (0...20).filter({ $0 != LottoNode.REQUIRED_DEPTH_FOR_SUCCESS })
        
        for depth in depthRange {
            // When
            let node : LottoNode = LottoNode(string: str, depth: depth)
            let isLottoNumber = node.ContainsLottoNumber()
            
            // Then
            XCTAssertFalse(isLottoNumber)
        }
        
    }
    
    func test_WhenNodeHasChildrenAndDepthLessThanRequiredDepth_ChildrenAreCheckedIfTheyAreALottoWinner() {
        //Given
        let child = LottoNodeMock()
        let node = LottoNode()
        node._children.append(child)

        for depth in 0..<(LottoNode.REQUIRED_DEPTH_FOR_SUCCESS)
        {
            child._didCheckIfIsWinningLottoNumber = false
            node._depth = depth
            
            // When
            node.ContainsLottoNumber()
            
            // Then
            XCTAssertTrue(child._didCheckIfIsWinningLottoNumber)
        }
    }
    
    func test_WhenNodeHasChildrenAndDepthGreaterOrEqualToRequiredDepth_ChildrenAreIgnored() {
        //Given
        let child = LottoNodeMock()
        let node = LottoNode()
        node._children.append(child)
        
        for depth in LottoNode.REQUIRED_DEPTH_FOR_SUCCESS...20
        {
            child._didCheckIfIsWinningLottoNumber = false
            node._depth = depth
            
            // When
            node.ContainsLottoNumber()
            
            // Then
            XCTAssertFalse(child._didCheckIfIsWinningLottoNumber)
        }
    }
    
    func test_WhenANodeHasAChildWhichContainsAWinningLottoNumber_NodeReturnsThatItContainsAWinningLottoNumber() {
        //Given
        let node = LottoNode()
        let child = LottoNodeMock()
        child._returnsIsWinningLottoNumber = true
        let childTwo = LottoNodeMock() // _returnsIsWinningLottoNumber = false by default
        node._children.append(child)
        node._children.append(childTwo)
        
        // When
        let containsLotto : Bool = node.ContainsLottoNumber()
        
        // Then
        XCTAssertTrue(child._didCheckIfIsWinningLottoNumber)
        XCTAssertTrue(containsLotto)
    }
    
    func test_WhenANodesChildContainsAWinningLottoNumber_NodeReturnsThatItContainsAWinningLottoNumber() {
        //Given
        let node = LottoNode()
        let child = LottoNodeMock()
        let childTwo = LottoNodeMock() // _returnsIsWinningLottoNumber = false by default
        node._children.append(child)
        node._children.append(childTwo)
        
        // When
        let containsLotto : Bool = node.ContainsLottoNumber()
        
        // Then
        XCTAssertTrue(child._didCheckIfIsWinningLottoNumber)
        XCTAssertTrue(childTwo._didCheckIfIsWinningLottoNumber)
        XCTAssertFalse(containsLotto)
    }

    func test_WhenANodeIsCreatedStartingWithAZero_ItWillHaveNoChildren() {
        let numExpectedChildren = 0
        for i in 0...9
        {
            //Given
            let nodeString = "0" + String(i)
            let node = LottoNode(string: nodeString, depth: 0)
            // When
            // initialization has already happened, so nothing need be done here
            // Then
            XCTAssertTrue(node._children.count == numExpectedChildren)
        }
    }
    
    func test_WhenANodeIsCreatedWith1Through5FollowedBy0_ItWillHaveOneChild() {
        let numExpectedChildren = 1
        for i in 1...5
        {
            //Given
            let stringStart = String(i) + "0"
            let nodeString = GetLottoStringOfLengthStartingWithString(start: stringStart, totalLength: LottoNode.REQUIRED_DEPTH_FOR_SUCCESS)
            let node = LottoNode(string: nodeString, depth: 0, value: -1, parent: nil, singlesRequired: 10, doublesRequired: 10)
            // When
            // initialization has already happened, so nothing need be done here
            // Then
            XCTAssertTrue(node._children.count == numExpectedChildren)
        }
    }

    
    func test_WhenANodeIsCreatedWith1Through5FollowedBy1Through9_ItWillHaveTwoChildren() {
        let numExpectedChildren = 2
        for i in 1...5
        {
            for j in 1...9
            {
                //Given
                let stringStart = String(i) + String(j)
                let nodeString = GetLottoStringOfLengthStartingWithString(start: stringStart, totalLength: LottoNode.REQUIRED_DEPTH_FOR_SUCCESS)
                let node = LottoNode(string: nodeString, depth: 0, value: -1, parent: nil, singlesRequired: 10, doublesRequired: 10)
                // When
                // initialization has already happened, so nothing need be done here
                // Then
                XCTAssertTrue(node._children.count == numExpectedChildren)
            }
        }
    }
    
    func test_WhenANodeIsCreatedWithA6Through9FollowedByA0_ItWillHaveNoChildren() {
        let numExpectedChildren = 0
        for i in 6...9
        {
            //Given
            let stringStart = String(i) + "0"
            let nodeString = GetLottoStringOfLengthStartingWithString(start: stringStart, totalLength: LottoNode.REQUIRED_DEPTH_FOR_SUCCESS)
            let node = LottoNode(string: nodeString, depth: 0)
            // When
            // initialization has already happened, so nothing need be done here
            // Then
            XCTAssertTrue(node._children.count == numExpectedChildren)
        }
    }
    
    func test_WhenANodeIsCreatedWithA6Through9FollowedByA1Through9_ItWillHaveOneChild() {
        let numExpectedChildren = 1
        for i in 6...9
        {
            for j in 1...9
            {
                //Given
                let stringStart = String(i) + String(j)
                let nodeString = GetLottoStringOfLengthStartingWithString(start: stringStart, totalLength: LottoNode.REQUIRED_DEPTH_FOR_SUCCESS)
                let node = LottoNode(string: nodeString, depth: 0)
                // When
                // initialization has already happened, so nothing need be done here
                // Then
                XCTAssertTrue(node._children.count == numExpectedChildren)
            }
        }
    }
    
    // NOTE: This is assuming a required depth of seven, so if we ever change that this will have to be slightly re-written
    func test_WhenANodeIsAskedForItsRepresentationAsALottoNumber_ItReturnsASpaceSeparatedListOfAllValuesLeadingUpToItStartingFromItsOldestAncestor()
    {
        // Given
        let firstNode = LottoNode(string: "1234567", depth: 0)
        let lastChild = firstNode._children[0]._children[0]._children[0]._children[0]._children[0]._children[0]._children[0]
        
        // When
        let lottoString = lastChild.AsLottoString()
        
        // Then
        XCTAssertEqual(lottoString, "1 2 3 4 5 6 7")
    }
    
    // NOTE: This is assuming a required depth of seven, so if we ever change that this will have to be slightly re-written
    func test_WhenARootNodeIsCreated_ItWillContainAllLegalLottoNumbers() {
        // Given
        let root = LottoNode(string: "12345671", depth: 0)
        
        // When
        // everything has already happened in initialization
        
        // Then
        XCTAssertTrue(root._legalLottoNumbers.contains("12 3 4 5 6 7 1"))
        XCTAssertTrue(root._legalLottoNumbers.contains("1 23 4 5 6 7 1"))
        XCTAssertTrue(root._legalLottoNumbers.contains("1 2 34 5 6 7 1"))
        XCTAssertTrue(root._legalLottoNumbers.contains("1 2 3 45 6 7 1"))
        XCTAssertTrue(root._legalLottoNumbers.contains("1 2 3 4 56 7 1"))
    }
    
    func test_WhenARootNodeIsCreated_ItHasTheCorrectNumberOfSingleAndDoublesRequired()
    {
        // Given
        let baseString = GetStringFromOneToInt(lastNumInList: LottoNode.REQUIRED_DEPTH_FOR_SUCCESS)
        
        
        for i in 1...LottoNode.REQUIRED_DEPTH_FOR_SUCCESS
        {
            let fullString = baseString + GetStringFromOneToInt(lastNumInList: i)
            let expectedSinglesRequired  = LottoNode.REQUIRED_DEPTH_FOR_SUCCESS - (fullString.characters.count - LottoNode.REQUIRED_DEPTH_FOR_SUCCESS)
            let expectedDoublesRequired = fullString.characters.count - LottoNode.REQUIRED_DEPTH_FOR_SUCCESS
            
            // When
            let root = LottoNode(string: fullString, depth: 0)
            
            // Then
            XCTAssertEqual(root._singlesRequired, expectedSinglesRequired)
            XCTAssertEqual(root._doublesRequired, expectedDoublesRequired)
        }
    }
    
    
    func test_WhenAChildIsCreatedFromASingleDigit_ItHasOneLessSingleRequiredThanItsParent()
    {
        // Given
        let root = LottoNode(string: "11111111111", depth: 0)
        let child = root._children[0]
 
        // Then
        XCTAssertEqual(child._singlesRequired, root._singlesRequired - 1)
        XCTAssertEqual(child._doublesRequired, root._doublesRequired)
    }
    
    func test_WhenAChildIsCreatedFromADoubleDigit_ItHasOneLessDoubleRequiredThanItsParent()
    {
        // Given
        let root = LottoNode(string: "10111111111", depth: 0)
        let child = root._children[0]
        
        // Then
        XCTAssertEqual(child._singlesRequired, root._singlesRequired)
        XCTAssertEqual(child._doublesRequired, root._doublesRequired - 1)
    }
    
    func test_WhenAChildWouldBeCreatedWithASingleDigitButOurNodeDoesntRequireAnymoreSingles_TheChildIsNotCreated()
    {
        // Given
        let lottoString = GetStringFromOneToInt(lastNumInList: LottoNode.REQUIRED_DEPTH_FOR_SUCCESS)
        let node = LottoNode(string: lottoString, depth: 0, value: -1, parent: nil, singlesRequired: 0, doublesRequired: 1)
        
        // Then
        XCTAssertEqual(node._children.count, 1)
        XCTAssertEqual(node._children[0]._value, 12)
    }
    
    func test_WhenAChildWouldBeCreatedWithADoubleDigitButOurNodeDoesntRequireAnymoreDoubles_TheChildIsNotCreated()
    {
        // Given
        let lottoString = GetStringFromOneToInt(lastNumInList: LottoNode.REQUIRED_DEPTH_FOR_SUCCESS)
        let node = LottoNode(string: lottoString, depth: 0, value: -1, parent: nil, singlesRequired: 1, doublesRequired: 0)
        
        // Then
        XCTAssertEqual(node._children.count, 1)
        XCTAssertEqual(node._children[0]._value, 1)
    }
    
    func test_WhenARootIsCreatedWithAStringLengthLessThanTheRequireDepthForSuccess_NoChildrenAreCreated()
    {
        // Given
        // Get string of length we require for success - 1; too short to ever find a solution
        let lottoString = GetStringFromOneToInt(lastNumInList: LottoNode.REQUIRED_DEPTH_FOR_SUCCESS - 1)
        let node = LottoNode(string: lottoString, depth: 0, value: -1, parent: nil, singlesRequired: 1, doublesRequired: 0)
        
        // Then
        XCTAssertEqual(node._children.count, 0)
    }
    
    func test_WhenARootIsCreatedWithAStringLengthGreaterThanTwiceTheRequireDepthForSuccess_NoChildrenAreCreated()
    {
        // Given
        // Get string of length we require for success - 1; too short to ever find a solution
        let lottoString = GetStringFromOneToInt(lastNumInList: LottoNode.REQUIRED_DEPTH_FOR_SUCCESS*2 + 1)
        let node = LottoNode(string: lottoString, depth: 0, value: -1, parent: nil, singlesRequired: 1, doublesRequired: 0)
        
        // Then
        XCTAssertEqual(node._children.count, 0)
    }
    
    private func GetStringFromOneToInt(lastNumInList : Int) -> String
    {
        var numList = ""
        for i in 1...lastNumInList {
            numList += String(i)
        }
        return numList
    }
    
    private func GetLottoStringOfLengthStartingWithString(start: String, totalLength : Int) -> String
    {
        var numList = start
        for i in 1...(totalLength - start.characters.count) {
            numList += String(i)
        }
        return numList
    }
}
