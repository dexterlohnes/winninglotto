//
//  LottoNode.swift
//  UnclesLotto
//
//  Created by Dexter Lohnes on 11/17/16.
//  Copyright © 2016 DexterLohnes. All rights reserved.
//

import Foundation

class LottoNode {
    
    var _value : Int = -1  // The value of this node (1-59 are allowed)
    var _string : String = "" // Our string from which we will be creating child nodes. Any solution node must have an empty string
    var _depth : Int = -1  // How far we are from the root. 0 == the root. Any solution node must be at depth 7
    var _singlesRequired : Int = -1 // Number of single digit numbers we must add to this branch to have a legal solution.
    var _doublesRequired : Int = -1 // Number of double digit numbers we must add to this branch to have a legal solution.
    var _children : [LottoNode] = []
    var _parent : LottoNode?
    var _legalLottoNumbers : [String] = [] // An array of strings representing the legal / solution lotto numbers of which this node is a part
    
    static let REQUIRED_DEPTH_FOR_SUCCESS = 7
    
    init()
    {
        initialization()
    }
    
    func initialization()
    {
        // If we weren't given singles required and doubles required by our parent at construction time, calculate them now
        // This should only ever get called by a root node
        if(_singlesRequired < 0 || _doublesRequired < 0)
        {
            CalculateSinglesAndDoublesRequired(lottoString: _string)
        }
        
        // Record winning lotto number if we are a winner
        if(_string == "" && _depth == LottoNode.REQUIRED_DEPTH_FOR_SUCCESS)
        {
            RecordLottoNumber(legalLottoNumber: AsLottoString())
        }
        
        // If we are the root node and our number of characters is too short or too long, don't bother making children
        if(_parent != nil || ((_string.characters.count >= LottoNode.REQUIRED_DEPTH_FOR_SUCCESS) && _string.characters.count <= LottoNode.REQUIRED_DEPTH_FOR_SUCCESS * 2))
        {
            CreateChildrenFromString(remainingLottoString: _string)
        }
        
    }
    
    init(string: String, depth : Int)
    {
        _string = string
        _depth = depth
        initialization()
    }
    
    init(string : String, depth : Int, value : Int, parent : LottoNode?, singlesRequired : Int, doublesRequired : Int)
    {
        _value = value
        _parent = parent
        _string = string
        _depth = depth
        _singlesRequired = singlesRequired
        _doublesRequired = doublesRequired
       initialization()
    }
    
    
    @discardableResult func ContainsLottoNumber() -> Bool
    {
        // If we are not yet deep enough but we have children, go ahead and check them
        if(_depth < LottoNode.REQUIRED_DEPTH_FOR_SUCCESS && _children.isEmpty == false)
        {
            for child in _children
            {
                let childContainsLottoNumber : Bool  = child.ContainsLottoNumber()
                if childContainsLottoNumber == true
                {
                    return true
                }
            }
        }
        else if(_depth == LottoNode.REQUIRED_DEPTH_FOR_SUCCESS && _string == "")
        {
            return true
        }
        else // Under any other circumstances, return false
        {
            return false
        }
        // We get here if our for loop over the children ends without finding a true value
        return false
    }
    
    func AsLottoString() -> String
    {
        var currentAncestor : LottoNode? = _parent
        var lottoString = String(_value)
        
        while(currentAncestor != nil && currentAncestor!._value > 0)
        {
            lottoString = String(currentAncestor!._value) + " " + lottoString
            currentAncestor = currentAncestor!._parent
        }
        
        return lottoString
    }
    
    func RecordLottoNumber(legalLottoNumber : String)
    {
        _legalLottoNumbers.append(legalLottoNumber)
        if(_parent != nil)
        {
            _parent?.RecordLottoNumber(legalLottoNumber: legalLottoNumber)
        }
    }
    
    private func CreateChildrenFromString(remainingLottoString : String)
    {
        // If for any reason this gets called more than once we don't want to put too many children in, so we always remove all children and NON-DESTRUCTIVELY read from the _string member
        _children.removeAll(keepingCapacity: false)
        
        TryToCreateChildWithPrefixOfString(lottoString : remainingLottoString, numPrefixCharacters: 1)
        TryToCreateChildWithPrefixOfString(lottoString : remainingLottoString, numPrefixCharacters: 2)
    }
    
    private func TryToCreateChildWithPrefixOfString(lottoString : String, numPrefixCharacters : Int)
    {
        if(lottoString.characters.count > (numPrefixCharacters - 1))
        {
            // create a number just using the first character from the string
            let valueString = String(lottoString.characters.prefix(numPrefixCharacters))
            // If our valueString starts with a 0, we will never extra a value from it
            if(valueString.characters.first == "0")
            {
                return
            }
            let value = Int(valueString)
            if(IsLegalNodeValue(val: value!))
            {
                let remainingString = String(lottoString.characters.dropFirst(numPrefixCharacters))
                // If the remainder of the string would start with a 0, that means this child can never be a legal solution so we should give up now by not adding the child
                if(remainingString.characters.first != "0")
                {
                    // If we are creating a single digit number, only proceed if require more single digit numbers. Otherwise, give up on it. Likewise, do the same for if it's double digit
                    let valueIsSingleDigit = (value! < 10)
                    let valueIsDoubleDigit = !valueIsSingleDigit
                    if((valueIsSingleDigit && _singlesRequired > 0) || (valueIsDoubleDigit && _doublesRequired > 0))
                    {
                        AddChildWithStringAndValue(lottoString: remainingString, value: value!)
                    }
                }
            }
        }
    }
    
    private func AddChildWithStringAndValue(lottoString : String, value : Int)
    {
        // If our value is less than 10 it's a single digit. Otherwise, a double digit
        let valueIsSingleDigit = (value < 10)
        let childSinglesRequired = valueIsSingleDigit ? _singlesRequired - 1 : _singlesRequired
        let childDoublesRequired = valueIsSingleDigit ? _doublesRequired : _doublesRequired - 1
        let newChild = LottoNode(string: lottoString, depth: _depth + 1, value: value, parent: self, singlesRequired: childSinglesRequired, doublesRequired: childDoublesRequired)
        self._children.append(newChild)
    }

    private func IsLegalNodeValue(val : Int) -> Bool
    {
        return val <= 59 && val >= 1
    }
    
    private func CalculateSinglesAndDoublesRequired(lottoString : String)
    {
        let characterCount = lottoString.characters.count
        let doublesRequired = characterCount - LottoNode.REQUIRED_DEPTH_FOR_SUCCESS
        let singlesRequired = LottoNode.REQUIRED_DEPTH_FOR_SUCCESS - doublesRequired
        _singlesRequired = singlesRequired
        _doublesRequired = doublesRequired
    }
    
}
