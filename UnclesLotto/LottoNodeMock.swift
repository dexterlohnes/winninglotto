//
//  LottoNodeMock.swift
//  UnclesLotto
//
//  Created by Dexter Lohnes on 11/17/16.
//  Copyright © 2016 DexterLohnes. All rights reserved.
//

import Foundation

class LottoNodeMock : LottoNode
{
    
    var _didCheckIfIsWinningLottoNumber = false
    var _returnsIsWinningLottoNumber = false
    
    override func ContainsLottoNumber() -> Bool
    {
        _didCheckIfIsWinningLottoNumber = true
        return _returnsIsWinningLottoNumber
    }

    
}
