//
//  main.swift
//  UnclesLotto
//
//  Created by Dexter Lohnes on 11/16/16.
//  Copyright © 2016 DexterLohnes. All rights reserved.
//

import Foundation

let argFileName = CommandLine.arguments[1]

if let path = Bundle.main.path(forResource: argFileName, ofType: nil) {
    do {
        let data = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
        let myStrings = data.components(separatedBy: "\n")
        for string in myStrings {
            let rootNode = LottoNode(string: string, depth: 0)
            for lotto in rootNode._legalLottoNumbers
            {
                print(lotto)
            }
        }
    } catch {
        print(error)
    }
} else {
    print("Could not find file")
}
